
import java.awt.Dimension;
import java.awt.Toolkit;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Utob
 */
public class MainFrame extends javax.swing.JFrame {
    
    public class Node
    {
        int data;
        int height;
        Node right;
        Node left;
        
        Node (int data)
        {
            this.data=data;
            this.height=1;
        }
        
    }
    
        private Node root;
        
        int nodeCount=0;
        
        
        
 //=========================================================================================================================
        
        
        private void insert(int data)
    {
          this.root = insert(this.root, data);
          nodeCount++;
    }
    
    private Node insert(Node node,int data)
    {
        
        if(node == null)
        {
            Node newnode = new Node(data);
            return newnode;
            
        }
            
        if(node.data < data)
        {
           node.right = insert(node.right, data);
        }
        else if(node.data > data){
            node.left = insert(node.left,data);
        }
        
        
        node.height = Math.max(height(node.left),height(node.right)) + 1;
        
        int bf = bf(node);
        
        //halate LL
        if(bf>1 && data< node.left.data)
        {
            return rightrotate(node);
        }
        
        // halate RR
        if(bf < -1 && data > node.right.data)
        {
            return leftrotate(node);
        }
        
        //halate LR
        if(bf > 1 && data > node.left.data)
        {
            node.left = leftrotate(node.left);
            return rightrotate(node);
        }
        
        //halate RL
        if(bf < -1 && data < node.right.data)
        {
            node.right = rightrotate(node.right);
            return leftrotate(node);
        }
        
      
        return node;
    }

    public int height(Node node)
    {
        if(node == null)
        {
            return 0;
        }
        
        return node.height;
        
    }
    
    
    public int bf(Node node)
    {
        if(node == null){
            return 0;
        }
        
        return height(node.left) - height(node.right);
    }
    
    public Node rightrotate(Node node)
    {
        Node b  =node.left;
        Node T3 =b.right;
        
        //Rotate
        b.right = node;
        node.left = T3;
        
        
        //height update
        node.height = Math.max(height(node.left), height(node.right)) + 1;
        b.height = Math.max(height(b.left), height(b.right)) + 1;
        
        return b;
    }
    
    
    public Node leftrotate(Node node)
    {
        Node b  =node.right;
        Node T2 =b.left;
        
        //Rotate
        b.left = node;
        node.right = T2;
        
        //height update
        node.height = Math.max(height(node.left), height(node.right)) + 1;
        b.height = Math.max(height(b.left), height(b.right)) + 1;
        
        return b;
    }
    
    
    public boolean contains(int data) {
    return contains(root, data);
  }

  // Recursive contains helper method.
  private boolean contains(Node node, int data) {
    
    if (node == null) return false;

    // Compare current value to the value in the node.
    int cmp = Integer.compare(data, node.data);
    // Dig into left subtree.
    if (cmp < 0) return contains(node.left, data);

    // Dig into right subtree.
    if (cmp > 0) return contains(node.right, data);

    // Found value in tree.
    return true;

  }
    
  
  public boolean remOve(int elem)
  {
      
    if (contains(root, elem)) {
      root = remove(root, elem);
      nodeCount--;
      return true;
    }

    return false;
  }
  
   

  // Removes a value from the AVL tree.
  private Node remove(Node node, int elem) {
    
    if (node == null) return null;
    
    int cmp = Integer.compare(elem, node.data);

    // Dig into left subtree, the value we're looking
    // for is smaller than the current value.
    if (cmp < 0) {
      node.left = remove(node.left, elem);

    // Dig into right subtree, the value we're looking
    // for is greater than the current value.
    } else if (cmp > 0) {
      node.right = remove(node.right, elem);

    // Found the node we wish to remove.
    } else {

      // This is the case with only a right subtree or no subtree at all. 
      // In this situation just swap the node we wish to remove
      // with its right child.
      if (node.left == null) {
        return node.right;
        
      // This is the case with only a left subtree or 
      // no subtree at all. In this situation just
      // swap the node we wish to remove with its left child.
      } else if (node.right == null) {
        return node.left;

      // When removing a node from a binary tree with two links the
      // successor of the node being removed can either be the largest
      // value in the left subtree or the smallest value in the right 
      // subtree. As a heuristic, I will remove from the subtree with
      // the most nodes in hopes that this may help with balancing.
      } else {

        // Choose to remove from left subtree
        if (node.left.height > node.right.height) {

          // Swap the value of the successor into the node.
          int successorValue = findMax(node.left);
          node.data = successorValue;

          // Find the largest node in the left subtree.
          node.left = remove(node.left, successorValue);

        } else {
  
          // Swap the value of the successor into the node.
          int successorValue = findMin(node.right);
          node.data = successorValue;

          // Go into the right subtree and remove the leftmost node we
          // found and swapped data with. This prevents us from having
          // two nodes in our tree with the same value.
          node.right = remove(node.right, successorValue);
        }
      }
    }

    // Update balance factor and height values.
    update(node);

    // Re-balance tree.
    return balance(node);

  }
  
  
  private Node balance(Node node) {

    // Left heavy subtree.
    if (bf(node) == -2) {

      // Left-Left case.
      if (bf(node.left) <= 0) {
        return rightrotate(node);
        
      // Left-Right case.
      } else {
          node.left = leftrotate(node.left);
            return rightrotate(node);
        
      }

    // Right heavy subtree needs balancing.
    } else if (bf(node) == +2) {

      // Right-Right case.
      if (bf(node.right) >= 0) {
         return leftrotate(node);

      // Right-Left case.
      } else {
          node.right = rightrotate(node.right);
            return leftrotate(node);
      }

    }

    // Node either has a balance factor of 0, +1 or -1 which is fine.
    return node;

  }

  
   private void update(Node node) {
    
    int leftNodeHeight  = (node.left  == null) ? -1 : node.left.height;
    int rightNodeHeight = (node.right == null) ? -1 : node.right.height;

    // Update this node's height.
    node.height = 1 + Math.max(leftNodeHeight, rightNodeHeight);

    // Update balance factor.
        bf(node);

  }

  // Helper method to find the leftmost node (which has the smallest value)
  private int findMin(Node node) {
    while(node.left != null) 
      node = node.left;
    return node.data;
  }

  // Helper method to find the rightmost node (which has the largest value)
  private int findMax(Node node) {
    while(node.right != null) 
      node = node.right;
    return node.data;
  }
  
  
  
  
  
    
    public void display()
    {
        display(this.root);
    }
    
    private void display(Node node) 
    {
        
        if(node == null)return ;
        String str = "";
        
        if(node.left == null)
        {
            str+=".";
        }
        else
        {
          str += node.left.data;
        }
        
        str += "==>" + node.data + "<==";
        
        if(node.right == null)
        {
            str+=".";
        }
        else
        {
          str += node.right.data;
        }
        
        System.out.print(str);
        display(node.left);
        display(node.right);
        
    }
    
    
    


    /**
     * Creates new form MainFrame
     */
    public MainFrame() {
        initComponents();
        
        Dimension dim = Toolkit.getDefaultToolkit().getScreenSize();
        this.setLocation(dim.width/2-this.getSize().width/2, dim.height/2-this.getSize().height/2);
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jPanel2 = new javax.swing.JPanel();
        insert = new javax.swing.JButton();
        delete = new javax.swing.JButton();
        insertText = new javax.swing.JTextField();
        deleteText = new javax.swing.JTextField();
        Error = new javax.swing.JLabel();
        show = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        jPanel1.setBackground(new java.awt.Color(0, 51, 51));

        jLabel1.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/uni_logo.png"))); // NOI18N
        jLabel1.setToolTipText("");

        jLabel2.setFont(new java.awt.Font("Arabic Typesetting", 0, 75)); // NOI18N
        jLabel2.setForeground(new java.awt.Color(255, 255, 255));
        jLabel2.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel2.setText("AVL Tree");

        jLabel3.setFont(new java.awt.Font("Calisto MT", 0, 35)); // NOI18N
        jLabel3.setForeground(new java.awt.Color(255, 255, 255));
        jLabel3.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel3.setText("Kharazmi University");

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jLabel2, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addComponent(jLabel3, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, 366, Short.MAX_VALUE)
            .addComponent(jLabel1, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGap(70, 70, 70)
                .addComponent(jLabel2)
                .addGap(48, 48, 48)
                .addComponent(jLabel1)
                .addGap(27, 27, 27)
                .addComponent(jLabel3, javax.swing.GroupLayout.PREFERRED_SIZE, 67, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        jPanel2.setBackground(new java.awt.Color(0, 102, 102));

        insert.setBackground(new java.awt.Color(0, 153, 153));
        insert.setFont(new java.awt.Font("Calisto MT", 0, 29)); // NOI18N
        insert.setForeground(new java.awt.Color(255, 255, 255));
        insert.setText("Insert");
        insert.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                insertMouseClicked(evt);
            }
        });

        delete.setBackground(new java.awt.Color(0, 153, 153));
        delete.setFont(new java.awt.Font("Calisto MT", 0, 29)); // NOI18N
        delete.setForeground(new java.awt.Color(255, 255, 255));
        delete.setText("Delete");
        delete.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                deleteMouseClicked(evt);
            }
        });

        insertText.setBackground(new java.awt.Color(0, 102, 102));
        insertText.setFont(new java.awt.Font("Calisto MT", 0, 25)); // NOI18N
        insertText.setForeground(new java.awt.Color(255, 255, 255));
        insertText.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        insertText.setBorder(javax.swing.BorderFactory.createMatteBorder(0, 0, 2, 0, new java.awt.Color(255, 255, 255)));

        deleteText.setBackground(new java.awt.Color(0, 102, 102));
        deleteText.setFont(new java.awt.Font("Calisto MT", 0, 25)); // NOI18N
        deleteText.setForeground(new java.awt.Color(255, 255, 255));
        deleteText.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        deleteText.setBorder(javax.swing.BorderFactory.createMatteBorder(0, 0, 2, 0, new java.awt.Color(255, 255, 255)));

        Error.setFont(new java.awt.Font("Calisto MT", 1, 30)); // NOI18N
        Error.setForeground(new java.awt.Color(102, 0, 0));
        Error.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);

        show.setBackground(new java.awt.Color(0, 153, 153));
        show.setFont(new java.awt.Font("Calisto MT", 0, 30)); // NOI18N
        show.setForeground(new java.awt.Color(255, 255, 255));
        show.setText("Show");
        show.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                showMouseClicked(evt);
            }
        });

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addGap(160, 160, 160)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel2Layout.createSequentialGroup()
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(deleteText)
                            .addComponent(insertText, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.PREFERRED_SIZE, 301, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(160, 160, 160))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel2Layout.createSequentialGroup()
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addGroup(jPanel2Layout.createSequentialGroup()
                                .addComponent(insert, javax.swing.GroupLayout.PREFERRED_SIZE, 170, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(10, 10, 10))
                            .addComponent(delete, javax.swing.GroupLayout.PREFERRED_SIZE, 170, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(show, javax.swing.GroupLayout.PREFERRED_SIZE, 180, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(220, 220, 220))))
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addComponent(Error, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGap(390, 390, 390))
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addGap(119, 119, 119)
                .addComponent(insertText, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(insert, javax.swing.GroupLayout.PREFERRED_SIZE, 70, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(deleteText, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(delete, javax.swing.GroupLayout.PREFERRED_SIZE, 70, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(110, 110, 110)
                .addComponent(show, javax.swing.GroupLayout.PREFERRED_SIZE, 70, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(35, 35, 35)
                .addComponent(Error, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(49, 49, 49))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGap(0, 0, 0)
                .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents
            
    private void insertMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_insertMouseClicked
        // TODO add your handling code here:
        
        if(insertText.getText().isEmpty())
        {
            Error.setText("Please enter a number.");
        }
        else
        {
        insert(Integer.parseInt(insertText.getText()));
        }
        
    }//GEN-LAST:event_insertMouseClicked

    private void showMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_showMouseClicked
        // TODO add your handling code here:
        
        
       display();
        
    }//GEN-LAST:event_showMouseClicked

    private void deleteMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_deleteMouseClicked
        // TODO add your handling code here:
        
        remOve(Integer.parseInt(deleteText.getText()));
        
    }//GEN-LAST:event_deleteMouseClicked

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(MainFrame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(MainFrame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(MainFrame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(MainFrame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new MainFrame().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel Error;
    private javax.swing.JButton delete;
    private javax.swing.JTextField deleteText;
    private javax.swing.JButton insert;
    private javax.swing.JTextField insertText;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JButton show;
    // End of variables declaration//GEN-END:variables
}
