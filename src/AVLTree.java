import java.util.Iterator;
import java.util.Queue;
import javax.swing.Spring;

public class AVLTree
{

    
    
    public class Node
    {
        int data;
        int height;
        Node right;
        Node left;
        
        Node (int data)
        {
            this.data=data;
            this.height=1;
        }
        
    }
    
    private Node root;
    
    private void insert(int data)
    {
          this.root = insert(this.root, data);
    }
    
    private Node insert(Node node,int data)
    {
        
        if(node == null)
        {
            Node newnode = new Node(data);
            return newnode;
        }
            
        if(node.data < data)
        {
           node.right = insert(node.right, data);
        }
        else if(node.data > data){
            node.left = insert(node.left,data);
        }
        
        
        node.height = Math.max(height(node.left),height(node.right)) + 1;
        
        int bf = bf(node);
        
        //halate LL
        if(bf>1 && data< node.left.data)
        {
            return rightrotate(node);
        }
        
        // halate RR
        if(bf < -1 && data > node.right.data)
        {
            return leftrotate(node);
        }
        
        //halate LR
        if(bf > 1 && data > node.left.data)
        {
            node.left = leftrotate(node.left);
            return rightrotate(node);
        }
        
        //halate RL
        if(bf < -1 && data < node.right.data)
        {
            node.right = rightrotate(node.right);
            return leftrotate(node);
        }
        
      
        return node;
    }

    public int height(Node node)
    {
        if(node == null)
        {
            return 0;
        }
        
        return node.height;
        
    }
    
    
    public int bf(Node node)
    {
        if(node == null){
            return 0;
        }
        
        return height(node.left) - height(node.right);
    }
    
    public Node rightrotate(Node node)
    {
        Node b  =node.left;
        Node T3 =b.right;
        
        //Rotate
        b.right = node;
        node.left = T3;
        
        
        //height update
        node.height = Math.max(height(node.left), height(node.right)) + 1;
        b.height = Math.max(height(b.left), height(b.right)) + 1;
        
        return b;
    }
    
    
    public Node leftrotate(Node node)
    {
        Node b  =node.right;
        Node T2 =b.left;
        
        //Rotate
        b.left = node;
        node.right = T2;
        
        //height update
        node.height = Math.max(height(node.left), height(node.right)) + 1;
        b.height = Math.max(height(b.left), height(b.right)) + 1;
        
        return b;
    }
    
    
    
    public void display()
    {
        display(this.root);
    }
    
    private void display(Node node) 
    {
        
        if(node == null)return ;
        String str = "";
        
        if(node.left == null)
        {
            str+=".";
        }
        else
        {
          str += node.left.data;
        }
        
        str += "==>" + node.data + "<==";
        
        if(node.right == null)
        {
            str+=".";
        }
        else
        {
          str += node.right.data;
        }
        
        System.out.println(str);
        display(node.left);
        display(node.right);
        
    }
    
    
    
    public static void main(String[] args) 
    {
 
        MainFrame mainFrame = new MainFrame();
        mainFrame.show();
//        AVLTree avlt = new AVLTree();
//        
//        avlt.insert(1);
//        avlt.insert(10);
//        avlt.insert(5);
//        avlt.insert(20);
//        avlt.insert(25);
//        avlt.insert(32);
//        avlt.insert(8);
//        create by erfan goodboy 
//        
//        avlt.display();
    }
}
